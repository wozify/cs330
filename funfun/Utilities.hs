module Utilities where

import Data.List
import GHC.Exts(sortWith)
import Data.Bits(testBit)
import System.Random.Shuffle(shuffleM)
import System.Environment(getArgs)

froto :: [a] -> [(a,a)]
froto [] = []
froto list = zip list $ tail list

frotoByNearness :: [Integer] -> [(Integer,Integer)]
frotoByNearness = sortWith (\(a,b) -> abs $ (-) a b) . froto

nearestPair :: [Integer] -> (Integer,Integer)
nearestPair = head . frotoByNearness

mapButLast :: (a->a) -> [a] -> [a]
mapButLast func list = [func(a) | a <- init(list)] ++ [last(list)]

implode :: (Show a) => String -> [a] -> String
implode sep = filter(/='\'') . concat . mapButLast(++sep) . map(show)

normspace :: String -> String
normspace = concat . map(nubBy(\x _ -> x == ' ')) . group

data Direction = North | South | East | West deriving (Eq, Show)

move1 :: (Int, Int) -> Direction -> (Int, Int)
move1 (a, b) North = (a, b+1)
move1 (a, b) South = (a, b-1)
move1 (a, b) East = (a+1, b)
move1 (a, b) West = (a-1, b)

moveN :: (Int, Int) -> [Direction] -> (Int, Int)
moveN = foldl (move1)

intermoves :: (Int, Int) -> [Direction] -> [(Int, Int)]
intermoves = scanl (move1)
