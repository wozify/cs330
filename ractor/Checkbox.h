#ifndef CHECKBOX_H
#define CHECKBOX_H
#include "Widget.h"
#include <string>
#include <vector>
#include <functional>

class Checkbox : public Widget {
	private:
	std::string text = "";
	public:
	int x; 
	int y;
	bool checked;
	std::vector<std::function<void(int)>> Listeners;	

	Checkbox(int, int, std::string);
	bool IsChecked() const;
	void IsChecked(bool);
	void Draw();
	void AddListener(std::function<void(bool)>);
	void OnMouseClick(int, int);
};

#endif
