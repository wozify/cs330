#include <curses.h>
#include "Slider.h"

Slider::Slider(int x, int y, int max, int def) : Widget::Widget(x, x+max, y, y) {
	this->x = x;
	this->y = y;
	this->max = max;
	this->def = def;
}

int Slider::GetValue() const {
	return def;
}

int Slider::GetMax() const {
	return max;	
}

void Slider::Draw() {
	std::string slider = "";
	for(int i = 0; i < def; i++) {
		slider = slider + ".";
	}
	slider = slider + "o";
	for(int j = def; j < max; j++) {
		slider = slider + ".";
	}
	mvprintw(y, x, slider.c_str()); 
}

void Slider::AddListener(std::function<void(int)> Listener) {
	Listeners.push_back(Listener);
}

void Slider::OnMouseClick(int x, int y) {
	def = x;
	for(unsigned int i = 0; i < Listeners.size(); i++) {
		Listeners[i](def);
	}
}
