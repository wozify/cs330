#include <curses.h>
#include <string>
#include "Window.h"

Window::Window() : Widget(-1, -1, -1, -1) {
	initscr();
	noecho();
	curs_set(0);
	keypad(stdscr, TRUE);
	mousemask(BUTTON1_CLICKED, NULL);	
}

Window::~Window() {
	
}

void Window::Add(Widget* widget) {
	winvec.push_back (widget);
}

void Window::Draw() {
	clear();
	unsigned int i;
	for(i = 0; i < winvec.size(); i++) {
		winvec[i]->Draw();
	}
}

void Window::Loop() {
	stop = false;
	while(!stop) {
		Draw();
		int input = getch();
		if (input == 'q') {
			stop = true;
		}
		else if(input == KEY_MOUSE) {
			if(getmouse (&mouseinput) == OK) {
			int x = mouseinput.x;
			int y = mouseinput.y;			
			unsigned int i;
			for(i = 0; i < winvec.size(); i++) {
				if(winvec[i]->Contains(x, y)) {
					x = x - winvec[i]->GetLeft();
					y = y - winvec[i]->GetTop();
					winvec[i]->OnMouseClick(x, y);
				}
			}
			}
		}
		refresh();
	}
	endwin();
}

void Window::Stop() {
	stop = true;
}
