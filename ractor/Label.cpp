#include <curses.h>
#include <string>
#include "Label.h"
	Label::Label(int x, int y, std::string text) : Widget(x, (x+text.length()-1), y, y) {
	this->x = x;
	this->y = y;
	this->text = text;
}

void Label::Draw() {
	mvprintw(y, x, text.c_str());
}

void Label::SetText(const std::string newText) {
	this->right = x + newText.length()-1;
	this->text = newText;
}
