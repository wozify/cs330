#ifndef RECTANGLE_H
#define RECTANGLE_H

class Rectangle {
	private:
	int left;
	int right;
	int top;
	int bottom;
	int length;
	int width;

	public:
	Rectangle(int, int, int, int);
	
	int GetLeft() const;
	int GetRight() const;
	int GetTop() const;
	int GetBottom() const;

	void SetLeft(int);
	void SetRight(int);
	void SetTop(int);
	void SetBottom(int);

	int GetWidth() const;
	int GetHeight() const;

	bool Contains(int, int) const;

	bool Intersects(Rectangle) const;
};

#endif
