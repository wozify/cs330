#include <curses.h>
#include <algorithm>
#include "List.h"

using namespace std;
using std::string;
using std::vector;
using std::function;

List::List(int x, int y, vector<string> list) : 
	Widget::Widget(x, x, y, y+list.size()-1){
	this->x = x;
	this->y = y;
	this->list = list;
	largestSize = 0;	

	//Set width to size of largest text thingie
	for(unsigned int i = 0; i < list.size(); i++) {
		if(list[i].size() > largestSize) {
			largestSize = list[i].size();
		}
	}
	
	right = x+largestSize-1;
	
	SelectedIndex = -1;
	SelectedText = "";
}

int List::GetSelectedIndex() const{
	return SelectedIndex;
}

string List::GetSelected() const{
	
	return SelectedText;
}

void List::Draw() {
	//iterate through list, make y coord + i
	for(unsigned int i = 0; i < list.size(); i++) {
		mvprintw(y+i, x, list[i].c_str());
	}
}

void List::AddListener(function<void(int)> Listener) {
	Listeners.push_back(Listener);
}

void List::OnMouseClick(int x, int y) {
	if(SelectedIndex != y) {
		SelectedIndex = y;
		SelectedText = list[y];
	}
	else {
		SelectedIndex = -1;
		SelectedText = "";
	}	

	for(unsigned int i = 0; i < Listeners.size(); i++) {
		Listeners[i](GetSelectedIndex());
	}
}
