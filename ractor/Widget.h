#ifndef WIDGET_H
#define WIDGET_H

class Widget {
	private:

	public:
	int left;
	int right;
	int top;
	int bottom;

	Widget(int, int, int, int);
	
	virtual ~Widget();
	
	int GetLeft() const;
	int GetRight() const;
	int GetTop() const;
	int GetBottom() const;

	bool Contains(int, int) const;

	virtual void Draw();

	virtual void OnMouseClick(int, int);
};

#endif
