#include <iostream>
#include "Rectangle.h"
using namespace std;

Rectangle::Rectangle(int l, int r, int t, int b) {
	if(l <= r && t <= b) {
		left = l;
		right = r;
		top = t;
		bottom = b;
	}
	else {
		cout << "Error: left point bigger than right, or top bigger than bottom!" << endl;	
	}
}

int Rectangle::GetLeft() const {
	return left;
}

int Rectangle::GetRight() const {
	return right;
}

int Rectangle::GetTop() const {
	return top;
}

int Rectangle::GetBottom() const {
	return bottom;
}

void Rectangle::SetLeft(int l) {
	if(l <= right) {	
		left = l;
	}
}

void Rectangle::SetRight(int r) {
	if(left <= r) {
		right = r;
	}
}

void Rectangle::SetTop(int t) {
	if(t <= bottom) {
		top = t;
	}
}

void Rectangle::SetBottom(int b) {
	if(top <= b) {
		bottom = b;
	}
}

int Rectangle::GetWidth() const {
	return (right - left + 1);
}

int Rectangle::GetHeight() const {
	return (bottom - top + 1);
}

bool Rectangle::Contains(int x, int y) const {
	if(left <= x && x <= right && top <= y && y <= bottom) {
		return true;
	}
	else {
		return false;
	}
}

bool Rectangle::Intersects(Rectangle r) const {
	if(r.GetRight() < left || r.GetLeft() > right || r.GetTop() > bottom || r.GetBottom() < top) {
		return false;
	}
	else {
		return true;
	}
}


