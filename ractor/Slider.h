#ifndef SLIDER_H
#define SLIDER_H
#include <string>
#include <vector>
#include <functional>
#include "Widget.h"

class Slider : public Widget {
	private:

	public:
	int x; 
	int y;
	int max;
	int def;
	std::vector<std::function<void(int)>> Listeners;
	Slider(int, int, int, int);
	int GetValue() const;
	int GetMax() const;
	void Draw();
	void AddListener(std::function<void(int)>);
	void OnMouseClick(int, int);
};

#endif
