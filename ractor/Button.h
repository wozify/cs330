#ifndef BUTTON_H
#define BUTTON_H
#include "Widget.h"
#include <string>
#include <vector>
#include <functional>

class Button : public Widget {
	private:
	std::string text = "";
	public:
	int x; 
	int y;
	Button(int, int, std::string);
	void Draw();
	void AddListener(std::function<void()>);
	void OnMouseClick(int, int);
	std::vector<std::function<void()>> Listeners;
};

#endif
