#ifndef LIST_H
#define LIST_H
#include "Widget.h"
#include <string>
#include <vector>
#include <functional>

class List : public Widget {
	private:

	public:
	int x;
	int y;
	std::vector<std::string> list;
	int SelectedIndex;
	unsigned int largestSize;
	std::string SelectedText;

	std::vector<std::function<void(int)>> Listeners;
	
	List(int, int, std::vector<std::string>);
	
	int GetSelectedIndex() const;

	std::string GetSelected() const;

	void Draw();

	void AddListener(std::function<void(int)>);

	void OnMouseClick(int, int);
};

#endif
