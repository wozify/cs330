#ifndef WINDOW_H
#define WINDOW_H
#include <curses.h>
#include <vector>
#include <string>
#include "Widget.h"

class Window : public Widget {
	private:
		std::vector<Widget*> winvec;
		bool stop;
		MEVENT mouseinput;
	public:
	Window();
	virtual ~Window();
	void Add(Widget*);
	void Draw();
	void Loop();
	void Stop();
};

#endif
