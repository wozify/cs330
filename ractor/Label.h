#ifndef LABEL_H
#define LABEL_H
#include "Widget.h"

class Label : public Widget {
	private:
	std::string text = "";
	public:
	int x;
	int y;
	//std::string &text;
	
	Label(int, int, std::string);
	
	void Draw();

	void SetText(const std::string);
};

#endif
