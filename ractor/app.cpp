#include <iostream>
#include <map>
#include <string>
#include <vector>

#include "Rectangle.h"
#include "Button.h"
#include "List.h"
#include "Checkbox.h"
#include "Slider.h"
#include "Label.h"
#include "Window.h"

using std::vector;
using std::string;

int main() {
	Window w;
	
	string output = "";
	double num;
	Label *display = new Label(0, 0, output);
	Button *one = new Button(1, 1, "1");
	Button *two = new Button(3, 1, "2");
	Button *three = new Button(5, 1, "3");
	Button *four = new Button(1, 2, "4");
	Button *five = new Button(3, 2, "5");
	Button *six = new Button(5, 2, "6");
	Button *seven = new Button(1, 3, "7");
	Button *eight = new Button(3, 3, "8");
	Button *nine = new Button(5, 3, "9");
	Button *zero = new Button(3, 4, "0");
	Button *neg = new Button(5, 4, "-");
	Button *f = new Button(1, 5, "to f ");
	Button *c = new Button(1, 6, "to c ");
	Button *clear = new Button(1, 7, "clear");

	w.Add(display);
	w.Add(one);
	w.Add(two);
	w.Add(three);
	w.Add(four);
	w.Add(five);
	w.Add(six);
	w.Add(seven);
	w.Add(eight);
	w.Add(nine);
	w.Add(zero);
	w.Add(neg);
	w.Add(f);
	w.Add(c);
	w.Add(clear);

	one->AddListener([&]() {
		output = output + "1";
		display->SetText(output);
	});

	two->AddListener([&]() {
		output = output + "2";
		display->SetText(output);
	});

	three->AddListener([&]() {
		output = output + "3";
		display->SetText(output);
	});

	four->AddListener([&]() {
		output = output + "4";
		display->SetText(output);
	});

	five->AddListener([&]() {
		output = output + "5";
		display->SetText(output);
	});	

	six->AddListener([&]() {
		output = output + "6";
		display->SetText(output);
	});

	seven->AddListener([&]() {
		output = output + "7";
		display->SetText(output);
	});

	eight->AddListener([&]() {
		output = output + "8";
		display->SetText(output);
	});

	nine->AddListener([&]() {
		output = output + "9";
		display->SetText(output);
	});
	
	zero->AddListener([&]() {
		output = output + "0";
		display->SetText(output);
	});

	neg->AddListener([&]() {
		output = output + "-";
		display->SetText(output);
	});
	
	f->AddListener([&]() {
		num = atof(output.c_str());
		num = (num*1.8)+32;
		output = std::to_string(num);
		display->SetText(output);
		num = 0;
	});
	c->AddListener([&]() {
		num = atof(output.c_str());
		num = ((num-32)/1.8);
		output = std::to_string(num);
		display->SetText(output);
		num = 0;
	});
	clear->AddListener([&]() {
		output = "";
		display->SetText(output);
		num = 0;
	});

	w.Loop();

	return 0;
}
