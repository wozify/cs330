#include <curses.h>
#include <iostream>
#include "Button.h"

Button::Button(int x, int y, std::string text) : Widget(x, (x+text.length()-1), y, y){
	this->x = x;
	this->y = y;
	this->text = text;
}

void Button::Draw() {
	attron(A_REVERSE);
	mvprintw(y, x, text.c_str());
	attroff(A_REVERSE);
}

void Button::AddListener(std::function<void()> Listener) {
	Listeners.push_back(Listener);
}

void Button::OnMouseClick(int x, int y) {
	for(unsigned int i = 0; i < Listeners.size(); i++) {
		Listeners[i]();
	}	
}


