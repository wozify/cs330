#include <iostream>
#include "Widget.h"
using namespace std;

Widget::Widget(int l, int r, int t, int b) {
	if(l <= r && t <= b) {
		left = l;
		right = r;
		top = t;
		bottom = b;
	}
	else {
		cout << "Error: left point bigger than right, or top bigger than bottom!" << endl;	
	}
}

Widget::~Widget() {

}

int Widget::GetLeft() const {
	return left;
}

int Widget::GetRight() const {
	return right;
}

int Widget::GetTop() const {
	return top;
}

int Widget::GetBottom() const {
	return bottom;
}

bool Widget::Contains(int x, int y) const {
	if((left <= x && x <= right) && (top <= y && y <= bottom)) {
		return true;
	}
	else {
		return false;
	}
}

void Widget::Draw() {

}

void Widget::OnMouseClick(int x, int y) {

}
