#include <curses.h>
#include "Checkbox.h"
Checkbox::Checkbox(int x, int y, std::string text) : Widget(x, (x+text.length()+3), y, y){
	this->x = x;
	this->y = y;
	this->text = text;
	checked = false;
}

bool Checkbox::IsChecked() const{
	return checked;
}

void Checkbox::IsChecked(bool needsChecking) {
	checked = needsChecking;
}

void Checkbox::Draw() {
	std::string checkBoxText = "";
	if(checked) {
		checkBoxText = "[x] " + text;
	}
	else {
		checkBoxText = "[ ] " + text;
	}
	mvprintw(y, x, checkBoxText.c_str());
}

void Checkbox::AddListener(std::function<void(bool)> Listener) {
	Listeners.push_back(Listener);
}

void Checkbox::OnMouseClick(int x, int y) {
	checked = !checked;
	for(unsigned int i = 0; i < Listeners.size(); i++) {
		Listeners[i](checked);
	}	
}


